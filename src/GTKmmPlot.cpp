//============================================================================
// Name        : GTKmmPlot.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>

#include "IGUI.h"
#include "GTKMMGUI.h"
#include "Program.hpp"

using namespace std;

int main(int argc, char *argv[]) {
	Gtk::Application * app = Gtk::Application::create(argc, argv).release();
	Program * program = new Program(app);
	IGUI * gui = new GTKMMGUI(program);

	gui->GetWindow()->set_title("GTKmm program");
	app->run(*gui->GetWindow());

	delete(program);
	delete(gui);
	delete(app);
	return 0;
}
