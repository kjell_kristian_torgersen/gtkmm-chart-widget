/*
 * IGUI.h
 *
 *  Created on: Sep 23, 2016
 *      Author: kjell
 */

#ifndef IGUI_H_
#define IGUI_H_

#include <gtkmm/window.h>

class IGUI {
public:
	virtual Gtk::Window * GetWindow(void) = 0;
	virtual ~IGUI() {
	}
};

#endif /* IGUI_H_ */
