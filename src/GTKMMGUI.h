/*
 * GTKMMGUI.h
 *
 *  Created on: Sep 23, 2016
 *      Author: kjell
 */

#ifndef GTKMMGUI_H_
#define GTKMMGUI_H_

#include "IGUI.h"
#include <gtkmm.h>
#include <vector>
#include <iostream>
#include "Program.hpp"
#include "Chart.hpp"

class GTKMMGUI: public IGUI {

public:
	GTKMMGUI(Program * program);
	Gtk::Window * GetWindow(void);
	Gtk::MenuItem* CreateFileMenu();
	virtual ~GTKMMGUI();
private:
	Program * program;
	Gtk::Window window;
	Gtk::Box box0;
	Gtk::MenuBar menuBar;
	Chart chart;
};

#endif /* GTKMMGUI_H_ */
