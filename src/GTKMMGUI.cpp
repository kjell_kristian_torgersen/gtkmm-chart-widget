/*
 * GTKMMGUI.cpp
 *
 *  Created on: Sep 23, 2016
 *      Author: kjell
 */

#include <gtkmm.h>
#include "GTKMMGUI.h"
#include "Program.hpp"

GTKMMGUI::GTKMMGUI(Program * program) :
		program(program), box0(Gtk::ORIENTATION_VERTICAL), menuBar(), chart()
{
	menuBar.append(*CreateFileMenu());
	box0.pack_start(menuBar, Gtk::PACK_SHRINK);
	box0.pack_start(chart, Gtk::PACK_EXPAND_WIDGET);
	window.add(box0);
	window.set_size_request(320, 240);
	window.show_all();

	Series series1;
	Series series2;

	const int width = 640;
	const int height = 480;

	for (int i = 0; i < width; i+=8) {
		double x = (double) i;
		double y = (1.0 + std::sin(2 * 2.0*M_PI * (double) i / (double) width)) * (double) height / 2.0;
		double y2 = (1.0 + std::cos(2 * 2.0*M_PI * (double) i / (double) width)) * (double) height / 2.0;
		series1.points.push_back( { x, y });
		series2.points.push_back( { x, y2 });
	}
	series1.color = {0.0,0.0,1.0};
	series1.graphSize = 2.0;
	series1.knotSize = 3.0;

	series2.color = {1.0,0.0,0.0};
	series2.graphSize = 2.0;
	series2.knotSize = 3.0;

	chart.AddSeries(series1);
	chart.AddSeries(series2);
	chart.AutoScale();

}

Gtk::Window * GTKMMGUI::GetWindow(void)
{
	return &window;
}

Gtk::MenuItem* GTKMMGUI::CreateFileMenu()
{
	Gtk::MenuItem * fileItem = Gtk::manage(new Gtk::MenuItem("File"));
	Gtk::Menu * menu = Gtk::manage(new Gtk::Menu());
	Gtk::MenuItem * item;

	fileItem->set_submenu(*menu);
	item = Gtk::manage(new Gtk::MenuItem("New"));
	//item->signal_activate().connect(entry.callback);
	menu->append(*item);
	item = Gtk::manage(new Gtk::MenuItem("Open..."));
	//item->signal_activate().connect(entry.callback);
	menu->append(*item);
	item = Gtk::manage(new Gtk::MenuItem("Save"));
	//item->signal_activate().connect(entry.callback);
	menu->append(*item);
	item = Gtk::manage(new Gtk::MenuItem("Save as..."));
	//item->signal_activate().connect(entry.callback);
	menu->append(*item);
	item = Gtk::manage(new Gtk::MenuItem("Exit"));
	item->signal_activate().connect(sigc::mem_fun(program, &Program::Close));
	menu->append(*item);
	return fileItem;
}

GTKMMGUI::~GTKMMGUI()
{

}

