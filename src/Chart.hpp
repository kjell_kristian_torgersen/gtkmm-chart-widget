/*
 * Chart.hpp
 *
 *  Created on: Sep 24, 2016
 *      Author: kjell
 */

#ifndef CHART_HPP_
#define CHART_HPP_

#include <gtkmm/drawingarea.h>

typedef struct PointTag {
	double x;
	double y;
} Point;

typedef struct ColorTag {
	double r,g,b;
} Color;

typedef struct SeriesTag {
	double knotSize;
	double graphSize;
	Color color;
	std::vector<Point> points;
} Series;

class Chart: public Gtk::DrawingArea
{
public:
	Chart();
	void AddSeries(Series series);
	void Zoom(Point from, Point to);
	void AutoScale(void);
	virtual ~Chart();
protected:
	 bool on_draw(const Cairo::RefPtr<Cairo::Context>& cr) override;
private:
	 Color backColor;
	 Color gridColor;
	 std::vector<Series> allSeries;
	 Point from, to, grid;
};

#endif /* CHART_HPP_ */
