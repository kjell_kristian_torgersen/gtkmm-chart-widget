/*
 * Chart.cpp
 *
 *  Created on: Sep 24, 2016
 *      Author: kjell
 */

#include "Chart.hpp"
#include <cmath>

Chart::Chart() :
		backColor( { 1.0, 1.0, 1.0 }), grid({10,6}), gridColor({0.5,0.5,0.5})
{

}

Chart::~Chart()
{
	// TODO Auto-generated destructor stub
}

void Chart::AddSeries(Series series)
{
	allSeries.push_back(series);
}

void Chart::Zoom(Point from, Point to)
{
	this->from = from;
	this->to = to;
}

void Chart::AutoScale(void)
{
	double maxx = -INFINITY;
	double minx = +INFINITY;
	double maxy = -INFINITY;
	double miny = +INFINITY;
	for (auto series : allSeries) {
		for (Point p : series.points) {
			if (p.x > maxx) maxx = p.x;
			if (p.y > maxy) maxy = p.y;

			if (p.x < minx) minx = p.x;
			if (p.y < miny) miny = p.y;
		}
	}
	Zoom( { minx, miny }, { maxx, maxy });
}

bool Chart::on_draw(const Cairo::RefPtr<Cairo::Context>& cr)
{
	Gtk::Allocation allocation = get_allocation();
	const int width = allocation.get_width();
	const int height = allocation.get_height();

	double w = to.x - from.x;
	double h = to.y - from.y;

	cr->set_line_width(1.0);
	cr->set_source_rgb(backColor.r, backColor.g, backColor.b);
	cr->rectangle(0, 0, width, height);
	cr->fill();

	cr->set_source_rgb(gridColor.r, gridColor.g, gridColor.b);
	double dx = (double) width / grid.x;
	double dy = (double) height / grid.y;
	for (int i = 0; i < grid.x; i++) {
		cr->move_to(i * dx, 0);
		cr->line_to(i * dx, height);
	}
	for (int i = 0; i < grid.y; i++) {
		cr->move_to(0, i * dy);
		cr->line_to(width, i * dy);
	}
	cr->stroke();
	for (auto series : allSeries) {
		cr->set_line_width(series.graphSize);
		cr->set_source_rgb(series.color.r, series.color.g, series.color.b);
		double x = (double) width * (series.points[0].x - from.x) / w;
		double y = (double) height * (series.points[0].y - from.x) / h;
		cr->move_to(x, y);
		for (Point p : series.points) {
			double x = (double) width * (p.x - from.x) / w;
			double y = (double) height * (p.y - from.x) / h;

			cr->line_to(x, y);
			if (series.knotSize != 0) {
				cr->begin_new_sub_path();
				cr->arc(x, y, series.knotSize, 0, 2 * M_PI);
				cr->move_to(x, y);
			}
		}
		cr->stroke();
	}

	/*cr->set_line_width(2);
	 cr->set_source_rgb(0.0, 0.0, 1.0);
	 cr->move_to(0, height / 2.0);
	 for (int i = 0; i < width; i += 16) {
	 double x = (double)i;
	 double y = (1.0 + std::sin(2 * M_PI * (double) i / (double) width)) * (double) height / 2.0;
	 cr->line_to(x, y);
	 cr->begin_new_sub_path();
	 cr->arc(x,y,4,0,2*M_PI);
	 cr->move_to(x,y);
	 }
	 ;*/

	return true;
}
