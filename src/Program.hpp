/*
 * Program.hpp
 *
 *  Created on: Sep 23, 2016
 *      Author: kjell
 */

#ifndef PROGRAM_HPP_
#define PROGRAM_HPP_

#include <iostream>
#include <gtkmm.h>

class Program {
public:
	Program(Gtk::Application * app);
	void Close();
	void Connect(std::string serial, int baud);
	void Disconnect(void);
	virtual ~Program();
private:
	Gtk::Application * app;
};

#endif /* PROGRAM_HPP_ */
